package com.daioware.ai.expertSystem;

import static com.daioware.language.compiler.Symbol.Type.ASSIG_OPERATOR;
import static com.daioware.language.compiler.Symbol.Type.IDENTIFIER;
import static com.daioware.language.compiler.Symbol.Type.SEMICOLON;

import java.util.List;

import com.daioware.language.compiler.Assignment;
import com.daioware.language.compiler.Expression;
import com.daioware.language.compiler.Identifier;
import com.daioware.language.compiler.InvalidSymbolException;
import com.daioware.language.compiler.LexicalAnalyzer;
import com.daioware.language.compiler.LogicExpression;
import com.daioware.language.compiler.Node;
import com.daioware.language.compiler.Sentence;
import com.daioware.language.compiler.Symbol;
import com.daioware.language.compiler.SyntacticException;
import com.daioware.language.compiler.Unary;
import com.daioware.language.compiler.Symbol.Type;

public class SyntacticAnalizer {
	private List<Symbol> symbols;
	private int index;
	private int endIndex;
	public SyntacticAnalizer(LexicalAnalyzer lexicalAnalizer) throws InvalidSymbolException {
		lexicalAnalizer.parse();
		lexicalAnalizer.removeWhiteSpaces();
		symbols=lexicalAnalizer.getSymbols();
		symbols.add(new Symbol("$",Type.UNKNOWN));
		endIndex=symbols.size();
	}
	public Node parse() throws SyntacticException {
		Node node=program();
		check("$");
		return node;
	}
	private Node program() throws SyntacticException {
		return sentences();
	}
	private Node sentences() throws SyntacticException {
		Sentence sentence;
		//recursive way, it will need a prime method to validate sentences>=1
		/*
		if(!isCurrentSymbol("$")) {
			sentences=new Sentence(assignment(),sentences());
		}
		else {
			sentences=null;
		}
		*/
		//iterative way
		Sentence prev,currentSentence;
		if(!isCurrentSymbol("$")) {
			sentence=new Sentence(assignment(),null);
		}
		else {
			throw new SyntacticException("One or more sentences required");
		}
		prev=sentence;
		while(!isCurrentSymbol("$")) {
			currentSentence=new Sentence(assignment(),null);
			prev.setNext(currentSentence);
			prev=currentSentence;
		}
		return sentence;
	}

	private Node assignment() throws SyntacticException{
        Assignment asig=null;
        asig=new Assignment(expression(),null);
        check(ASSIG_OPERATOR);
        asig.setRight(expression());
        check(SEMICOLON);
        return asig;
    }
	private Expression expression() throws SyntacticException {
		return logicalExpression_OR();
	}
	
	private Expression logicalExpression_OR() throws SyntacticException{
		Expression exp=logicalExpression_AND();
		Expression prime=logicalExpression_OR_Prime();
		if(prime==null) {
			return exp;
		}
		else {
			prime.getLowestLeft().setLeft(exp);
			return prime;
		}
	}
	private Expression logicalExpression_OR_Prime() throws SyntacticException{
		Expression or;
		Expression prime;
		//recursive way
		/*
		 Expression and;
		if(isCurrentSymbol("||")) {
			nextSymbol();
			and=logicalExpression_AND();
			or=new LogicExpression(null,and,"||");
			prime=logicalExpression_OR_Prime();
			if(prime!=null) {
				prime.left=or;
				or=prime;
			}
		}
		else {
			or=null;
		}
		*/
		//iteration way
		or=null;
		while(isCurrentSymbol("||")) {
			nextSymbol();
			prime=new LogicExpression(or,logicalExpression_AND(),"||");
			or=prime;
		}
		return or;
	}
	private Expression logicalExpression_AND() throws SyntacticException{
		Expression exp=primaryExpression();
		Expression prime=logicalExpression_AND_Prime();
		if(prime==null) {
			return exp;
		}
		prime.getLowestLeft().setLeft(exp);
		return prime;
	}
	private Expression logicalExpression_AND_Prime() throws SyntacticException{
		Expression exp=null;
		while(isCurrentSymbol("&&")) {
			nextSymbol();
			exp=new LogicExpression(exp,primaryExpression(),"&&");
		}		
		return exp;
	}
	private Expression primaryExpression() throws SyntacticException {
		Expression exp;
		if(isCurrentType(IDENTIFIER)) {
			exp=new Identifier(getCurrentText());
			nextSymbol();
		}
		else if(isCurrentSymbol("!")) {
			nextSymbol();
			exp=new Unary(primaryExpression(),"!");
		}
		else {
			check("(");
			exp=expression();	
			check(")");
		}
		return exp;
	}
	private Type getCurrentType() {
		return getCurrentSymbol().getType();
	}
	private void check(Type type) throws SyntacticException {
		if(isCurrentType(type)) {
			nextSymbol();
		}
		else {
            throw new SyntacticException("I received "+getCurrentText()+":"+getCurrentType()
            	+", and I was waiting for "+type);
		}		
	}
	private void check(String textSymbol) throws SyntacticException {
		if(isCurrentSymbol(textSymbol)) {
			nextSymbol();
		}
		else {
            throw new SyntacticException("I was waiting for '"+textSymbol +"' and I received '"+getCurrentText()+"'");
		}		
	}
	private boolean isCurrentType(Type type) {
		Symbol currentSymbol=getCurrentSymbol();
        return currentSymbol!=null && currentSymbol.getType().equals(type);
	}
	private boolean isCurrentSymbol(String symb) {
		Symbol currentSymbol=getCurrentSymbol();
        return currentSymbol!=null && currentSymbol.getText().equals(symb);
	}
	
	private void nextSymbol() {
		index++;
	}
	private Symbol getCurrentSymbol() {
		if(index<endIndex){
            return symbols.get(index);
        }
        else{
            return null;
        }
	}
	private String getCurrentText() {
		Symbol s=getCurrentSymbol();
		return s!=null?s.getText():null;
	}
	public static void main(String[] args) {
		String text="red || c && yellow && a && b && c";
		text="a && b = c && d && z || p && x && w;";
		//"a b && = c d && z && p && x && w && ||"
		//text="a || b = c && b || d || d && d;";
		//text="a = b;";
		//text="a && b=c && d && x && y;";
		text="a=b;x=y;c=d;";
		LexicalAnalyzer lex=new LexicalAnalyzer(text);
		LexicalAnalyzer lex2=new LexicalAnalyzer(text);
		try {
			SyntacticAnalizer synt=new SyntacticAnalizer(lex);
			lex2.parse();
			lex2.removeWhiteSpaces();
			for(Object o:lex2) {
				System.out.println(o);
			}						
			Node node=synt.parse();
			System.out.println("Original:\n"+text);
			System.out.println("Postfix Node:\n"+node);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
