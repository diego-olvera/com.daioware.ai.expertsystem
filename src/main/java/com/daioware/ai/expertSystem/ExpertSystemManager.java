package com.daioware.ai.expertSystem;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.daioware.file.FileUtil;
import com.daioware.language.compiler.InvalidSymbolException;
import com.daioware.language.compiler.SyntacticException;
import com.daioware.stream.Printer;
import com.daioware.stream.PrinterItem;

public class ExpertSystemManager extends PrinterItem{
	
	public static final String exts[]= {".kb",".a"};

	private File mapFile;
	private KnowlegeBaseManager knowlegeManager;
	
	public ExpertSystemManager(File mapFile,Printer printer) {
		super(printer);
		knowlegeManager=new KnowlegeBaseManager();
		setMapFile(mapFile);
	}

	public File getMapFile() {
		return mapFile;
	}

	public void setMapFile(File mapFile) {
		this.mapFile = mapFile;
	}
	
	public boolean addKnowlegeBase(String name,File kbFile,File atomsFile) throws IOException, InvalidSymbolException, SyntacticException {
		KnowlegeBase kb=new KnowlegeBase(name,kbFile);
		if(kbFile==null) {
			println("KbFile is null");
			return false;
		}
		if(addKnowlegeBase(kb)) {
			println("Loaded '"+kb + "' from file '"+kbFile.getAbsolutePath()+"'");
			if(atomsFile!=null) {
				println("Loading atoms' file from "+atomsFile.getAbsolutePath());
				kb.updateAtomsInfo(atomsFile);
			}
			return true;
		}
		else {
			println("Could not load '"+kb + "' from file '"+kbFile.getAbsolutePath()+"'");
			return false;
		}
	}
	public void load() throws IOException, InvalidSymbolException, SyntacticException {
		knowlegeManager.clear();
		String[] lineSep;
		File mapFile=getMapFile();
		File atomsFile;
		File kbFile;
		if(mapFile.isFile()) {
			println("Loading knowlege base from map file "+mapFile.getAbsolutePath());
			for(String line:FileUtil.getContents(mapFile).split("\n")) {
				line=line.trim();
				lineSep=line.split(",");
				addKnowlegeBase(lineSep[0], FileUtil.getCompatibleFile(lineSep[1]), lineSep.length>=3?
						FileUtil.getCompatibleFile(lineSep[2]):null);
			}
		}
		else {
			println("Loading knowlege base from map directory "+mapFile.getAbsolutePath());
			for(File file:mapFile.listFiles()){
				atomsFile=null;
				kbFile=null;
				if(file.isDirectory()) {
					for(File f:FileUtil.getFiles(file,exts)) {
						if(f.getName().endsWith(".kb")) {
							kbFile=f;
						}
						else if(f.getName().endsWith(".a")) {
							atomsFile=f;
						}
					}
					if(kbFile!=null) {
						addKnowlegeBase(file.getName(), kbFile, atomsFile);
					}
				}
			}
		}
	}
	public boolean addKnowlegeBase(KnowlegeBase kb) {
		return knowlegeManager.add(kb);
	}
	public KnowlegeBase removeKnowlegeBase(String kbName) {
		return knowlegeManager.remove(kbName);
	}
	public KnowlegeBase getKnowlegeBase(String name) {
		return knowlegeManager.get(name);
	}
	public ExpertSystem getExpertSystem(String name) {
		KnowlegeBase kb=getKnowlegeBase(name);
		ExpertSystem expert;
		if( kb!=null) {
			expert=new ExpertSystem(kb.clone());
			expert.setPrinter(getPrinter());
			return expert;
		}
		else {
			return null;
		}
	}
	public List<KnowlegeBase> getKnowlegeBases(){
		return knowlegeManager.getKnowlegeBases();
	}
	
	@Override
	public String toString() {
		StringBuilder info=new StringBuilder();
		for(KnowlegeBase k:getKnowlegeBases()) {
			info.append("Knowlege base "+k.getName());
			for(Rule r:k.getRules()) {
				info.append(r).append("\n");
			}
		}
		return info.toString();
	}

	public static void main(String[] args) {
		ExpertSystemManager expMan=new ExpertSystemManager(new File("_es\\map.txt"),Printer.defaultPrinter);
		try {
			expMan.load();
			for(KnowlegeBase k:expMan.getKnowlegeBases()) {
				System.out.println("Knowlege base "+k);
				for(Rule r:k.getRules()) {
					System.out.println(r);
				}
			}
		} catch (IOException | InvalidSymbolException | SyntacticException e) {
			e.printStackTrace();
		}
	}
}
