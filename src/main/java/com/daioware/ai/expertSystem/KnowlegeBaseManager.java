package com.daioware.ai.expertSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KnowlegeBaseManager {
	private Map<String,KnowlegeBase> knowlegeBases;
	
	public KnowlegeBaseManager() {
		knowlegeBases=Collections.synchronizedMap(new HashMap<String,KnowlegeBase>());
	}
	public boolean add(KnowlegeBase k) {
		String name=k.getName();
		if(knowlegeBases.containsKey(name)) {
			return false;
		}
		else {
			knowlegeBases.put(name,k);
			return true;
		}
	}	
	public List<KnowlegeBase> getKnowlegeBases() {
		return new ArrayList<>(knowlegeBases.values());
	}
	public KnowlegeBase update(KnowlegeBase k) {
		return knowlegeBases.put(k.getName(),k);
	}
	public KnowlegeBase get(String name) {
		return knowlegeBases.get(name);
	}
	public KnowlegeBase remove(String name) {
		return knowlegeBases.remove(name);
	}
	public void clear() {
		knowlegeBases.clear();
	}
}
