package com.daioware.ai.expertSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.daioware.commons.InvalidNewAttributeException;
import com.daioware.language.compiler.Assignment;
import com.daioware.language.compiler.Expression;
import com.daioware.language.compiler.Identifier;
import com.daioware.language.compiler.InvalidEvaluation;
import com.daioware.language.compiler.Unary;

public class Rule implements Consumer<Object>{
	private Expression condition;
	private String plainTextRule;
	private List<Atom> conditions;
	private List<Atom> conclusions;
	private Boolean value;
	
	public Rule() {
		conclusions=new ArrayList<>();
		conditions=new ArrayList<>();
	}
	public Rule(Assignment assig) {
		this();
		setAssigment(assig);
	}
	public Rule(Rule rule) {
		condition=rule.getCondition();
		List<Atom> otherConclusions=rule.getConclusions();
		List<Atom> otherConditions=rule.getConditions();
		setConclusions(new ArrayList<>(otherConclusions.size()));
		setConditions(new ArrayList<>(otherConditions.size()));
		for(Atom atom:otherConclusions) {
			addConclusion(atom.clone());
		}
		for(Atom atom:otherConditions) {
			addCondition(atom.clone());
		}
		setPlainTextRule(rule.getPlainTextRule());
	}
	public Rule clone() {
		return new Rule(this);
	}
	
	public void setAssigment(Assignment assig) {
		setCondition(assig.getRight());
		setConclusions(assig.getLeft(),true);
		setConditions(assig.getRight());
	}
	
	public List<Atom> getConditions(){
		return conditions;
	}
	public List<Atom> getConclusions(){
		return conclusions;
	}
	
	public void setConditions(List<Atom> conditions) {
		this.conditions = conditions;
	}
	public void setConclusions(List<Atom> conclusions) {
		this.conclusions = conclusions;
	}
	public Atom getCondition(String name) {
		return find(conditions,name);
	}
	public Atom getConclusion(String name) {
		return find(conclusions,name);
	}
	protected Atom find(List<Atom> atoms,String name) {
		for(Atom a:atoms) {
			if(a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
	@Override
	public void accept(Object t) {
		String name=t.toString();
		try {
			conditions.add(new Atom(name));
		} catch (InvalidNewAttributeException e) {
			e.printStackTrace();
		}
	}
	protected void setConditions(Expression right) {
		right.consumeVars(this);
	}
	protected void setConclusions(Expression exp,boolean isTrueValue) {
		if(exp==null) {
			return;
		}
		if(exp instanceof Identifier) {
        	try {
				addConclusion(new Atom(exp.getText(),isTrueValue));
			} catch (InvalidNewAttributeException e) {
				e.printStackTrace();
			}
        	return;
        }
		isTrueValue=exp instanceof Unary?!exp.getOperator().equals("!"):true;
        setConclusions(exp.getLeft(),isTrueValue);           	
        setConclusions(exp.getRight(),isTrueValue);           	
	}
	public void setValue(Boolean value) {
		this.value = value;
	}
	public Boolean isValue(){
		return value;
	}
	protected boolean addConclusion(Atom r) {
		conclusions.add(r);
		return true;
	}
	protected boolean addCondition(Atom r) {
		conditions.add(r);
		return true;
	}
	public boolean hasConclusion(String name) {
		return find(conclusions,name)!=null;
	}
	public boolean hasObjectiveConclusion() {
		for(Atom a:getConclusions()){
			if(a.isObjective()) {
				return true;
			}
		}
		return false;
	}
	public String getPlainTextRule() {
		return plainTextRule;
	}
	public void setPlainTextRule(String plainTextRule) {
		this.plainTextRule = plainTextRule;
	}
	public boolean isTrue(Memory m) throws InvalidEvaluation {
		return getCondition().evaluate(m);
	}

	/**
	 * Makes true or false.
	 * @return true if is an objective rule, false otherwise
	 */
	public boolean makeTrue(Memory m) {
		boolean conclusion=false;
		for(Atom a:conclusions) {
			m.add(a);
			if(a.isObjective()) {
				conclusion=true;
			}
		}
		setValue(true);
		return conclusion;
	}
	public Expression getCondition() {
		return condition;
	}
	public void setCondition(Expression condition) {
		this.condition = condition;
	}
	public String getIfText() {
		return "If";
	}
	public String getThenText() {
		return "Then";
	}
	public String getConditionsText() {
		return "Conditions";
	}
	public static List<Atom> getConclusions(List<Atom> atoms) {
		List<Atom> conclusionsFound=new ArrayList<>();
		for(Atom atom:atoms) {
			if(atom.isObjective() && !conclusionsFound.contains(atom)) {
				conclusionsFound.add(atom);
			}
		}
		return conclusionsFound;
	}
	public String toString() {
		StringBuilder info=new StringBuilder();
		String sep,plainTextRule=getPlainTextRule();
		info.append("###Rule###\n");
		info.append(getConditionsText()).append(":\n\t");
		sep="";
		for(Atom atom:conditions) {
			info.append(sep);
			info.append(atom.getName());
			sep="\n\t";
		}
		info.append("\n");
		info.append(getIfText()).append(":\n\t");
		info.append(plainTextRule.substring(plainTextRule.indexOf("=")+1));
		info.append("\n").append(getThenText()).append(":\n\t");
		sep="";
		for(Atom atom:conclusions) {
			info.append(sep);
			info.append(atom.isObjective()?"*":"");
			info.append(atom.isValue()?"":"!").append(
					atom.getClearName());
			sep=" & ";
		}		
		return info.toString();
	}
}
