package com.daioware.ai.expertSystem;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.daioware.file.FileUtil;
import com.daioware.language.compiler.InvalidSymbolException;
import com.daioware.language.compiler.LexicalAnalyzer;
import com.daioware.language.compiler.Sentence;
import com.daioware.language.compiler.SyntacticException;

public class KnowlegeBase implements Iterable<Rule>{
	public static final Charset defaultCharset=Charset.forName("utf-8");
	private List<Rule> rules;
	private String name;
	
	public KnowlegeBase(String name,File file) throws InvalidSymbolException, SyntacticException, IOException {
		this(name,file,defaultCharset);
	}
	public KnowlegeBase(String name,File file,Charset charset) throws InvalidSymbolException, SyntacticException, IOException {
		this(name,FileUtil.getContents(file,charset));
	}
	public KnowlegeBase(String name,String rules) throws InvalidSymbolException, SyntacticException {
		this(name,getRules(rules));
	}
	public KnowlegeBase(List<Rule> rules) {
		this("Unknown",rules);
	}
	public KnowlegeBase(String name,List<Rule> rules) {
		setName(name);
		setRules(rules);
	}
	public KnowlegeBase(KnowlegeBase b) {
		List<Rule> otherRules=b.getRules();
		List<Rule> rules=new ArrayList<>(otherRules.size());
		for(Rule rule:otherRules) {
			rules.add(rule.clone());
		}
		setName(b.getName());
		setRules(rules);
	}
	public KnowlegeBase clone() {
		return new KnowlegeBase(this);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	@Override
	public Iterator<Rule> iterator() {
		return rules.iterator();
	}
	@Override
	public String toString() {
		return getName();
	}
	public void updateAtomsInfo(File file) throws IOException {
		updateAtomsInfo(FileUtil.getContents(file));
	}
	public void updateAtomsInfo(String atomsContent) {
		JsonElement root = new JsonParser().parse(atomsContent);
		JsonObject jsonAtom;
		String atomName;
		Atom atom;
		for(JsonElement jsonElement:root.getAsJsonArray()) {
			jsonAtom=jsonElement.getAsJsonObject();
			atomName=jsonAtom.get("name").getAsString();
			for(Rule rule:rules) {
				atom=rule.getCondition(atomName);
				if(atom==null) {
					atom=rule.getConclusion(atomName);
				}
				if(atom!=null) {
					atom.setClearName(jsonAtom.get("clearName").getAsString());
					atom.setDescription(jsonAtom.get("description").getAsString());
				}
			}
		}
	}
	public static List<Rule> getRules(String content) throws InvalidSymbolException, SyntacticException{
		int ruleCounter=0;
		String plainTextRules[]=content.split(";");
		ArrayList<Rule> rules=new ArrayList<>(plainTextRules.length);
		SyntacticAnalizer syntac=new SyntacticAnalizer(new LexicalAnalyzer(content));
		Sentence sentence=(Sentence)syntac.parse();
		Rule rule;
		while(sentence!=null) {
			rule=new Rule(sentence.data.toAssignment());
			rule.setPlainTextRule(plainTextRules[ruleCounter++].trim());
			rules.add(rule);
			sentence=(Sentence) sentence.getNext();
		}
		return rules;
	}
}
