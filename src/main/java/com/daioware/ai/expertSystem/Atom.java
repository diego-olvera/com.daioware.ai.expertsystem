package com.daioware.ai.expertSystem;

import com.daioware.commons.InvalidNewAttributeException;
import com.daioware.commons.MyJSONObject;
import com.daioware.commons.string.StringUtil;


public class Atom implements MyJSONObject{
	public static enum Type {
		CONCLUSION("Conclusion"),
		KNOWLEGE_BASE_CONCLUSION("Knowlege Base Conclusion"),
		REGULAR("Regular")
		;
		private String name;
		
		Type(String name){
			this.name=name;
		}
		public String toString() {
			return name;
		}
	}
	private String name;
	private String clearName;
	private String description;
	private int certainty;

	private Boolean value;
	private Type type;
	public Atom() {
		
	}
	public Atom(String name, String description, int certainty) throws InvalidNewAttributeException {
		setName(name);
		setClearName(name.replace("_"," "));
		setDescription(description);
		setCertainty(certainty);
	}
	public Atom(String name, String description) throws InvalidNewAttributeException {
		this(name,description,100);
	}
	public Atom(String name, String clearName,String description) throws InvalidNewAttributeException {
		this(name,description,100);
		setClearName(clearName);
	}
	public Atom(String name) throws InvalidNewAttributeException {
		this(name,"");
	}
	public Atom(String name,Boolean value) throws InvalidNewAttributeException {
		this(name,"");
		setValue(value);
	}
	public Atom(Atom atom) throws InvalidNewAttributeException {
		setName(atom.getName());
		setType(atom.getType());
		setClearName(atom.getClearName());
		setValue(atom.isValue());
		setCertainty(atom.getCertainty());
	}
	public Atom clone() {
		try {
			return new Atom(this);
		} catch (InvalidNewAttributeException e) {
			e.printStackTrace();
			return null;
		}
	}
	public String getName() {
		return name;
	}

	public Boolean isValue() {
		return value;
	}
	public void setValue(Boolean value) {
		this.value = value;
	}
	public void setName(String name) throws InvalidNewAttributeException{
		this.name = name;
		if(StringUtil.isValidVariableName(name)) {
			setType(name);
		}
		else {
			throw new InvalidNewAttributeException("Name not valid:"+name);
		}
	}
	public String getClearName() {
		return clearName;
	}
	public void setClearName(String clearName) {
		this.clearName = clearName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCertainty() {
		return certainty;
	}
	public void setCertainty(int certainty) {
		this.certainty = certainty;
	}
	public boolean isObjective() {
		Type type=getType();
		return type.equals(Type.CONCLUSION) || 
				type.equals(Type.KNOWLEGE_BASE_CONCLUSION);
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	protected void setType(String name) {
		if(StringUtil.isAllUpperCase(name)) {
			setType(Type.KNOWLEGE_BASE_CONCLUSION);
		}
		else if(Character.isUpperCase(name.charAt(0))) {
			setType(Type.CONCLUSION);
		}
		else {
			setType(Type.REGULAR);
		}
	}
	@Override
	public String toString() {
		return "Atom [name=" + name + ", clearName=" + clearName + ", description=" + description + ", certainty="
				+ certainty + ", type=" +  getType()+ ", value=" + value + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Atom))
			return false;
		Atom other = (Atom) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
}
