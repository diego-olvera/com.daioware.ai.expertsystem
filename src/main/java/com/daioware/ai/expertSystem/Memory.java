package com.daioware.ai.expertSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.daioware.commons.InvalidNewAttributeException;
import com.daioware.language.compiler.Evaluator;
import com.daioware.language.compiler.InvalidEvaluation;


public class Memory implements Evaluator, Iterable<Atom> {
	
	private Map<String,Atom> atoms;
	
	public Memory() {
		atoms=new HashMap<>();
	}
	
	public boolean add(Atom a) {
		String n=a.getName();
		if(atoms.containsKey(n)) {
			return false;
		}
		atoms.put(n,a);
		return true;
	}
	public Atom getAtomOrCreate(String name) throws InvalidNewAttributeException {
		Atom a=get(name);
		if(a==null) {
			a=new Atom(name);
			add(a);
		}
		return a;
	}
	public Atom get(String name) {
		return atoms.get(name);
	}
	public boolean contains(String name) {
		return atoms.containsKey(name);
	}
	public boolean contains(Atom a) {
		return atoms.containsKey(a.getName());
	}
	@Override
	public boolean isTrue(String name) throws InvalidEvaluation {
		Atom a=get(name);
		if(a==null) {
			throw new InvalidEvaluation(name+" is not in memory");
		}
		Boolean v=a.isValue();
		if(v!=null) {
			return v;
		}
		else {
			throw new InvalidEvaluation(name+" is not in memory");
		}
	}
	public List<Atom> getAtoms(){
		return new ArrayList<>(atoms.values());
	}
	@Override
	public Iterator<Atom> iterator() {
		return atoms.values().iterator();
	}
	public String toString() {
		StringBuilder info=new StringBuilder();
		for(Atom a:this) {
			info.append(a.getName()).append(":").append(a.isValue()).append("\n");
		}
		return info.toString();
	}
}
