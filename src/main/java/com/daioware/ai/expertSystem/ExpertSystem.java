package com.daioware.ai.expertSystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.daioware.commons.ScanF;
import com.daioware.file.FileUtil;
import com.daioware.language.compiler.InvalidEvaluation;
import com.daioware.stream.Printer;
import com.daioware.stream.PrinterItem;

public class ExpertSystem extends PrinterItem{
	protected KnowlegeBase knowlegeBase;
	protected List<Rule> rules;
	protected Stack<Rule> stackRules;
	protected Memory memory;
	
	public ExpertSystem(KnowlegeBase knowlegeBase) {
		this(knowlegeBase,Printer.defaultPrinter);
	}
	public ExpertSystem(KnowlegeBase knowlegeBase,Printer p) {
		super(p);
		setKnowlegeBase(knowlegeBase);		
	}
	
	public KnowlegeBase getKnowlegeBase() {
		return knowlegeBase;
	}
	public void setKnowlegeBase(KnowlegeBase knowlegeBase) {
		this.knowlegeBase = knowlegeBase;
		rules=knowlegeBase.getRules();
		memory=new Memory();
		stackRules=new Stack<>();
		startStackRules();
	}
	protected void startStackRules() {
		for(Rule r:rules) {
			if(r.hasObjectiveConclusion()) {
				stackRules.push(r);
			}
		}	
	}
	public Memory getMemory() {
		return memory;
	}

	public Atom nextQuestion() {
		Atom atomQ=null;
		Rule rule=null;
		Rule ruleFound;
		while(!stackRules.isEmpty()) {
			atomQ=null;
			rule=null;
			while(!stackRules.isEmpty()) {
				rule=stackRules.peek();
				if(rule.isValue()!=null) {
					rule=null;
					stackRules.pop();
				}
				else {
					break;
				}
			}
			if(rule==null) {
				return null;
			}
			for(Atom c:rule.getConditions()) {
				if(!memory.contains(c)) {
					atomQ=c;
					break;
				}
			}
			if(atomQ==null) {
				println("All conditions in:-- "+rule.getPlainTextRule()+" -- are in memory");
				stackRules.pop();
			}
			else {
				ruleFound=searchAtomInConclusions(atomQ.getName());
				if(ruleFound!=null) {
					//complex atom
					stackRules.push(ruleFound);
					println("Found a rule that has the conclusion "+atomQ.getName());
					println("Rule:"+ruleFound);				
				}
				else {
					//simple atom
					break;
				}
			}
		}
		return atomQ;		
	}
	
	public List<Rule> getRules() {
		return rules;
	}
	public List<Atom> getSimpleAtoms(){
		List<Atom> atoms=new ArrayList<>();
		for(Rule r:rules) {
			for(Atom c:r.getConditions()) {
				if(!atoms.contains(c) && searchAtomInConclusions(c.getName())==null) {
					atoms.add(c);
				}
			}
		}
		return atoms;
	}
	public synchronized Rule answer(Atom atom) {
		//Not sure if it is right to check if it as a conclusion
		/*Rule rule=searchAtomInConclusions(atom.getName());
		Atom aux;
		if(rule!=null) {
			aux=rule.getConclusion(atom.getName());
			if(aux!=null) {
				atom=aux;
			}
		}*/
		if(memory.add(atom)) {
			println("New Atom in memory:"+atom);
			return checkAnswers();
		}
		else {
			println("Atom already in memory:"+atom);
			return null;
		}
		
	}
	public Rule checkAnswers() {
		Boolean b;
		boolean atLeastOneRuleActivated;
		String plainTextRule;
		do {
			atLeastOneRuleActivated=false;
			for(Rule rule:rules) {
				b=rule.isValue();
				if(b==null) {
					plainTextRule=rule.getPlainTextRule();
					println("Evaluating rule "+plainTextRule);
					try {
						if(rule.isTrue(memory)) {
							println("Rule '"+plainTextRule+"' true");
							if(rule.makeTrue(memory)) {
								return rule;
							}
						}
						else {
							rule.setValue(false);
							println("Rule '"+plainTextRule+"' is false");
						}
						atLeastOneRuleActivated=true;
					} catch (InvalidEvaluation e) {
						println(e.getMessage());
					}
				}
				//else the rule it's already true or false
			}
		}while(atLeastOneRuleActivated);
		
		return null;
	}
	public Rule searchAtomInConclusions(String name) {
		Rule rule;
		Boolean isTrue;
		for(int i=0,j=rules.size();i<j;i++) {
			rule=rules.get(i);
			isTrue=rule.isValue();
			if(isTrue==null && rule.hasConclusion(name)) {
				return rule;
			}
		}
		return null;
	}
	public static void main(String[] args) {
		File file=FileUtil.getCompatibleFile("_es/se1/bc.kb");
		try {
			ExpertSystem exp=new ExpertSystem(new KnowlegeBase("test", file));
			Atom atom;
			System.out.println("Starting SE");
			boolean question;
			Rule resultRule=null;
			System.out.println("Rules");
			for(Rule r:exp.getKnowlegeBase().getRules()) {
				System.out.println(r);
			}
			System.out.println("Simple atoms");
			for(Atom r:exp.getSimpleAtoms()) {
				System.out.println(r);
			}
			//exp.answer(new Atom("SerCaballero"));
			while((atom=exp.nextQuestion())!=null){
				System.out.print("Question:"+atom.getName());
				question=ScanF.readString().equalsIgnoreCase("y");
				resultRule=exp.answer(new Atom(atom.getName(),question));
				System.out.println(exp.getMemory());
				if(resultRule!=null) {
					break;
				}
			}
			if(resultRule!=null) {
				System.out.println("SUCCESS:\n"+resultRule);
			}
			else {
				System.out.println("Failed!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
