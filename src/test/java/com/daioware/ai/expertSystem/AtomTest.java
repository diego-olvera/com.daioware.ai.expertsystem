package com.daioware.ai.expertSystem;

import static org.junit.Assert.*;

import org.junit.Test;

import com.daioware.ai.expertSystem.Atom;
import com.daioware.commons.InvalidNewAttributeException;

public class AtomTest {

	@Test
	public void test() throws InvalidNewAttributeException {
		Atom atom=new Atom("is_red","Color red property");
		Atom atom2=new Atom("IS_BLUE","Color blue property");
		Atom atom3=new Atom("Is_green","Color green property");
		assertEquals(atom.getType(),Atom.Type.REGULAR);
		assertEquals(atom2.getType(),Atom.Type.KNOWLEGE_BASE_CONCLUSION);
		assertEquals(atom3.getType(),Atom.Type.CONCLUSION);

		System.out.println(atom);
		System.out.println(atom2);
		System.out.println(atom3);
	}
}
